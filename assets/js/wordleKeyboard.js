import { createIcons, SkipBack, LogIn } from 'lucide';

const container = document.getElementById("KeyboardContainer");

export function generateKeyboard(layout) {
    if (layout === "azerty") {
        const azerty = [
            ['A', 'Z', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'],
            ['Q', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M'],
            ['W', 'X', 'C', 'V', 'B', 'N', 'Backspace', 'Enter']
        ]

        generate(azerty);
    } else if (layout === "qwerty") {
        const qwerty = [
            ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'],
            ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Enter'],
            ['Z', 'X', 'C', 'V', 'B', 'N', 'M', 'Backspace']
        ]

        generate(qwerty);
    }

    createIcons({
        icons: {
            SkipBack,
            LogIn,
        },
    });

    generateKeyboardSwitch(layout);
}

function generate(keyboard) {
    keyboard.forEach(line => {
        let lineElement = document.createElement("div");
        lineElement.classList.add("keyboard__line");
        container.appendChild(lineElement);

        line.forEach((key, j) => {
            let keyElement = document.createElement("button");
            keyElement.classList.add("keyboard__key");
            lineElement.appendChild(keyElement);

            if (key === 'Enter') {
                let iconElement = document.createElement("icon");
                iconElement.setAttribute("icon-name", "log-in");
                keyElement.appendChild(iconElement);
            } else if (key === 'Backspace') {
                let iconElement = document.createElement("icon");
                iconElement.setAttribute("icon-name", "skip-back");
                keyElement.appendChild(iconElement);
            } else {
                keyElement.textContent = line[j];
            }

            document.body.addEventListener('keydown', e => {
                if (keyElement.textContent.toLowerCase() === e.key || key === e.key) {
                    keyElement.classList.add("enabled");
                }
            })

            document.body.addEventListener('keyup', e => {
                if (keyElement.textContent.toLowerCase() === e.key || key === e.key) {
                    keyElement.classList.remove("enabled");
                }
            })
        })
    })
}

function generateKeyboardSwitch(layout) {
    const div = document.createElement('div');
    div.classList.add("keyboard__switch-container");
    const button = document.createElement('button');
    button.classList.add("keyboard__switch");
    container.insertAdjacentElement('beforeend', div);
    const switchContainer = document.querySelector(".keyboard__switch-container");
    switchContainer.appendChild(button);

    button.addEventListener('click', () => {
        if (layout === "qwerty") {
            container.replaceChildren();
            generateKeyboard("azerty");
            button.textContent = "Switch to QWERTY";
        } else if (layout === "azerty") {
            container.replaceChildren();
            generateKeyboard("qwerty");
            button.textContent = "Switch to AZERTY";
        }
    })

    layout === "qwerty" ? button.textContent = "Switch to AZERTY" : button.textContent = "Switch to QWERTY"
}