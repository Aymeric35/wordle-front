import { setGameParameters } from "./gameParameters";
import { generateGrid } from "./wordleGrid"
import { generateKeyboard } from "./wordleKeyboard";

setGameParameters();
generateGrid(6, 6, false);
generateKeyboard("qwerty");