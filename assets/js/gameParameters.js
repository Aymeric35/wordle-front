import { generateGrid } from "./wordleGrid";

export function setGameParameters() {
    const button = document.getElementById("submitOptions");
    button.addEventListener('click', () => {
        const inputs = document.getElementById("gameOptions").elements;
        const length = inputs["wordLength"].value;
        const attempts = inputs["nbOfAttempts"].value;
        generateGrid(length, attempts, true);
    })
}