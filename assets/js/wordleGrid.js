export function generateGrid(length, attempts, isRegenerated) {
    if (length > 12 || attempts > 15) return

    const container = document.getElementById("wordleContainer");
    if (isRegenerated) container.replaceChildren();

    for (let i = 0; i < attempts; i++) {
        let line = document.createElement("div");
        line.classList.add("wordle__line")
        container.appendChild(line);
    }

    const lines = document.querySelectorAll(".wordle__line");

    lines.forEach(line => {
        for (let i = 0; i < length; i++) {
            let square = document.createElement("div");
            square.classList.add("wordle__letter");
            line.appendChild(square);
        }
    })
}
